// 1. Animal Class
class Animal {
    constructor(name){
        this._name = name
        this._legs = 4
        this._cold_blooded = false
    }
    get name(){
        return this._name
    }
    set name(nama){
        this._name = nama
    }
    get legs(){
        return this._legs
    }
    set legs(jml){
        this._legs = jml
    }
    get cold_blooded(){
        return this._cold_blooded
    }
    set cold_blooded(darah){
        this._cold_blooded = darah
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Frog extends Animal {
    constructor(name){
        super(name)
    }
    jump(){
        console.log(`hop hop`);
    }
}

class Ape extends Animal {
    constructor(name){
        super(name)
        this._legs = 2
    }
    yell(){
        console.log(`Auooo`);
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
// console.log(sungokong.legs);

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 




// 2. Function to Class
class Clock {
    // Code di sini
    constructor({template}){
        this.template = template
        this.timer 
    }
    render(){
        return () => {
            var date = new Date();
    
            var hours = date.getHours();
            if (hours < 10) hours = '0' + hours;
    
            var mins = date.getMinutes();
            if (mins < 10) mins = '0' + mins;
    
            var secs = date.getSeconds();
            if (secs < 10) secs = '0' + secs;

            var output = this.template
                .replace('h', hours)
                .replace('m', mins)
                .replace('s', secs);
    
            console.log(output);
        }
    }
    start(){
        this.timer = setInterval(this.render(), 1000);
    }
    stop(){
        clearInterval(this.timer);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  
