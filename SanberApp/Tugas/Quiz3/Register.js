import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  Button,
  TouchableOpacity,
} from 'react-native';

const Register = ({navigation}) => {
  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={{marginBottom: 37}}>
          <Text style={styles.welcome}>Welcome</Text>
          <Text style={styles.underWelcome}>Sign up to Continue</Text>
        </View>
        {/* BOX INPUT */}
        <View style={styles.boxReg}>
          <View style={{marginHorizontal: 16}}>
            <Text style={styles.name}>Name</Text>
            <TextInput style={styles.inputText} />
            <View style={styles.line} />
          </View>
          <View style={{marginHorizontal: 16}}>
            <Text style={styles.name}>Email</Text>
            <TextInput style={styles.inputText} />
            <View style={styles.line} />
          </View>
          <View style={{marginHorizontal: 16}}>
            <Text style={styles.name}>Phone number</Text>
            <TextInput style={styles.inputText} />
            <View style={styles.line} />
          </View>
          <View style={{marginHorizontal: 16}}>
            <Text style={styles.name}>Password</Text>
            <TextInput secureTextEntry={true} style={styles.inputText} />
            <View style={styles.line} />
          </View>
          <View style={{margin: 20}}>
            <Button title="Sign Up" borderRadius={6} color="#F77866" onPress={() => navigation.push('Login')} />
            <Text
              style={{
                textAlign: 'center',
                fontFamily: 'Montserrat',
                fontSize: 12,
                marginTop: 10,
              }}>
              Already have an account?
              <TouchableOpacity onPress={() => navigation.push('Login')}>
                <Text
                  style={{
                    fontFamily: 'Montserrat',
                    fontSize: 12,
                    fontWeight: '500',
                    marginTop: 10,
                    color: '#F77866',
                  }}>
                  {' '}
                  Sign in
                </Text>
              </TouchableOpacity>
            </Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Register;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    margin: 25,
  },
  welcome: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 30,
    color: '#0C0423',
  },
  underWelcome: {
    fontFamily: 'Montserrat',
    fontSize: 12,
    color: '#4D4D4D',
  },
  boxReg: {
    flex: 1,
    borderRadius: 11,
    shadowColor: '#000',
    borderWidth: 0.1,
    paddingVertical: 30,
    // shadowOffset: {
    //   width: 0,
    //   height: 1,
    // },
    // shadowOpacity: 0.18,
    // shadowRadius: 1,
    // elevation: 1,
  },
  name: {
    marginTop: 10,
    fontFamily: 'Montserrat',
    fontSize: 12,
    color: '#4D4D4D',
  },
  inputText: {
    fontFamily: 'Montserrat',
    fontSize: 15,
    color: '#4C475A',
  },
  line: {
    height: 2,
    backgroundColor: '#E6EAEE',
  },
});
