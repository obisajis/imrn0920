import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  Button,
  TouchableOpacity,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const Login = ({navigation}) => {
  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={{marginBottom: 37}}>
          <Text style={styles.welcome}>Welcome Back</Text>
          <Text style={styles.underWelcome}>Sign in to Continue</Text>
        </View>
        {/* BOX INPUT */}
        <View style={styles.boxReg}>
          <View style={{marginHorizontal: 16}}>
            <Text style={styles.name}>Email</Text>
            <TextInput style={styles.inputText} />
            <View style={styles.line} />
          </View>
          <View style={{marginHorizontal: 16}}>
            <Text style={styles.name}>Password</Text>
            <TextInput secureTextEntry={true} style={styles.inputText} />
            <View style={styles.line} />
            <TouchableOpacity style={{marginTop: 10, marginBottom: 20}}>
              <Text style={{textAlign: 'right'}}>Forgot Password?</Text>
            </TouchableOpacity>
          </View>
          <View style={{margin: 20}}>
            <Button title="Sign In" borderRadius={6} color="#F77866" onPress={() => navigation.push('Home')} />
            <View style={{alignItems: 'center', marginVertical: 30}}>
              <Text>-OR-</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-around',
              }}>
              <TouchableOpacity style={styles.socmed}>
                <Icon name="facebook" size={16} />
                <Text style={{marginLeft: 5}}>Facebook</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.socmed}>
                <Icon name="google" size={16} />
                <Text style={{marginLeft: 5}}>Google</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    margin: 25,
  },
  welcome: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 30,
    color: '#0C0423',
  },
  underWelcome: {
    fontFamily: 'Montserrat',
    fontSize: 12,
    color: '#4D4D4D',
  },
  boxReg: {
    flex: 1,
    borderRadius: 11,
    shadowColor: '#000',
    borderWidth: 0.1,
    paddingVertical: 30,
    // shadowOffset: {
    //   width: 0,
    //   height: 1,
    // },
    // shadowOpacity: 0.18,
    // shadowRadius: 1,
    // elevation: 1,
  },
  name: {
    marginTop: 10,
    fontFamily: 'Montserrat',
    fontSize: 12,
    color: '#4D4D4D',
  },
  inputText: {
    fontFamily: 'Montserrat',
    fontSize: 15,
    color: '#4C475A',
  },
  line: {
    height: 2,
    backgroundColor: '#E6EAEE',
  },
  socmed: {
    padding: 10,
    borderRadius: 6,
    borderWidth: 1,
    color: '#E6EAEE',
    flexDirection: 'row',
  },
});
