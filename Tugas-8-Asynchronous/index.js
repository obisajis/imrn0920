// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
let waktu = 10000
let idx = 0
function baca(waktu, books){
    if(idx < books.length){
        readBooks(waktu, books[idx], sisa => {
            idx++
            baca(sisa, books)
        })
    }
}

baca(waktu, books)
